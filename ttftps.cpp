#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>

#include "WRQ.h"
#include "ACK.h"
#include "Data.h"

using namespace std;

int port_number = 0;
const int WAIT_FOR_PACKET_TIMEOUT = 3;
const int NUMBER_OF_FAILURES = 7;
int error_count = 0;
FILE *pfile;

int main(int argc, char* argv[])
{
	int newsockfd;
	int recieved_packet_len;
	char read_buff[516] = {0};
	char ack_buff[5] = {0};
	int timeoutExpiredCount;
	unsigned int client_addr_len;
	struct sockaddr_in client_addr = {0};
	WRQ req;
	Data data;

	if (argc != 2)
	{
		printf("Error in parameters\n");
	}

	port_number = atoi(argv[1]);

	//configure socket
	int sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	printf("sockfd = %d\n", sockfd);
	if (sockfd == -1)
	{
		printf("sockfd = %d\n", sockfd);
		exit(1);
	}
	//bind
	struct sockaddr_in addr = {0};
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port_number);

	int bind_return = bind(sockfd, (struct sockaddr *)& addr, sizeof(addr));
	printf("bind_return = %d\n", bind_return);

	if (bind_return < 0)
	{
		printf("TTFTP_ERROR:");
		perror(NULL);
		exit(1);
	}
/*
	//set non-blocking mode to socket
	int flags = fcntl(sockfd, F_GETFL);
	flags = flags+O_NONBLOCK;
	if(flags < 0)
	{
		printf("TTFTP_ERROR:");
		printf("unable to retrieve descriptor status flags");
		exit(1);
	}
	if(fcntl(sockfd, F_SETFL, flags)<0)
	{
		printf("TTFTP_ERROR:");
		printf("unable to set descriptor status flags");
		exit(1);
	}
*/
	//listen
//	int listen_return = listen(sockfd,1);
//	if (listen_return != 0)
//	{
//		printf("TTFTP_ERROR:");
//		perror(NULL);
//		printf("listen returned %d\n", listen_return);
//		exit(1);
//	}

//	//new socket = accept
//	unsigned int client_addr_len;
//	struct sockaddr_in client_addr = {0};
//	int newsockfd = accept(sockfd, (struct sockaddr *)& client_addr, &client_addr_len);
//	if (newsockfd < 0)
//	{
//		printf("ERROR on accept\n");
//	}

	//wait for WRQ
	fd_set rfds;
	struct timeval tv;
	printf("hi\n");

	//follow newsockfd for writes
	printf("hi\n");

	printf("hi\n");

	printf("port number = %d\n", port);
	//do wait for connection while not reached max time
	do
	{
		//wait for WAIT_FOR_PACKET_TIMEOUT sec interval
		FD_ZERO(&rfds);
		FD_SET(sockfd, &rfds);
		tv.tv_sec = WAIT_FOR_PACKET_TIMEOUT;
		tv.tv_usec = 0;
		int select_return = select(1, &rfds, NULL, NULL, &tv);
		printf("select_return = %d\n", select_return);
		//if connection is established
		if (select_return > 0)
		{
			//new socket = accept
			//unsigned int client_addr_len;
			newsockfd = accept(sockfd, (struct sockaddr *)& client_addr, &client_addr_len);
			if (newsockfd < 0)
			{
				printf("ERROR on accept\n");
				exit(1);
			}

			//recv WRQ
			//char read_buff[516] = {0};
			recieved_packet_len = recvfrom(newsockfd, read_buff, sizeof(read_buff), 0, (struct sockaddr *)& client_addr, &client_addr_len);
			if (req.CheckValidReq(read_buff, recieved_packet_len) == false)
			{
				printf("ERROR bad write request packet\n");
			}

			//send ACK0
			ACK ack(0);
			ack.AckToChar(ack_buff);

			sendto(newsockfd, ack_buff, sizeof(ack_buff), 0, (struct sockaddr *)& client_addr, client_addr_len);

			break;
		}

		error_count++;

		if (error_count>= NUMBER_OF_FAILURES)
		{
			// FATAL ERROR BAIL OUT
			printf("ERROR\n");
		}
	} while (error_count < NUMBER_OF_FAILURES);
	//} while (recieved_packet_len <= 0); // TODO: Continue while some socket was ready
	// but recvfrom somehow failed to read the data

	if (error_count == NUMBER_OF_FAILURES)
	{
		printf("FLOWERROR: Exeeded NUMBER_OF_FAILURES in select\n");
		exit(1);
	}

	pfile = fopen((char*)req.file_name, "wb");

	//if we reached here, connection is established and ACK0 was sent
	do
	{
		int i = 0;
		do
		{
			do
			{
				// TODO: Wait WAIT_FOR_PACKET_TIMEOUT to see if something appears
				// for us at the socket (we are waiting for DATA)
				int select_return = select(sockfd, &rfds, NULL, NULL, &tv);

				//if there was something at the socket and
				// we are here not because of a timeout
				if (select_return == -1)
				{
					printf("ERROR in select\n");
				}

				else if (select_return > 0)
				{
					//new socket = accept
					//unsigned int client_addr_len;
					//client_addr = {0};
					newsockfd = accept(sockfd, (struct sockaddr *)& client_addr, &client_addr_len);
					if (newsockfd < 0)
					{
						printf("ERROR on accept\n");
						exit(1);
					}

					// TODO: Read the DATA packet from the socket (at
					// least we hope this is a DATA packet)
					//recv DATAi
					//char read_buff[516] = {0};
					recieved_packet_len = recvfrom(newsockfd, read_buff, sizeof(read_buff), 0, (struct sockaddr *)& client_addr, &client_addr_len);

				}

				else if (select_return == 0) // TODO: Time out expired while waiting for data
				// to appear at the socket
				{

					//TODO: Send another ACK for the last packet
					//send ACK i
					ACK ack(i);
					ack.AckToChar(ack_buff);

					sendto(newsockfd, ack_buff, sizeof(ack_buff), 0, (struct sockaddr *)& client_addr, client_addr_len);

					timeoutExpiredCount++;

				}

				if (timeoutExpiredCount>= NUMBER_OF_FAILURES)
				{
					// FATAL ERROR BAIL OUT
					printf("ERROR\n");
				}

			} while (recieved_packet_len <= 0); // TODO: Continue while some socket was ready
			// but recvfrom somehow failed to read the data

			if (data.is_data_valid(read_buff, recieved_packet_len) == false) // TODO: We got something else but DATA
			{
				// FATAL ERROR BAIL OUT
				printf("ERROR: bad data packet\n");
				exit(1);
			}

			if (atol(strcat(data.block_num, "\0")) == i+1) // TODO: The incoming block number is not what we have
			// expected, i.e. this is a DATA pkt but the block number
			// in DATA was wrong (not last ACK’s block number + 1)
			{
				// FATAL ERROR BAIL OUT
				printf("ERROR: bad data block num\n");
				exit(1);
			}
		} while (false);

		timeoutExpiredCount = 0;
		recieved_packet_len = fwrite(read_buff, sizeof(char), recieved_packet_len, pfile); // Data is perfectly fine so write next bulk of data
		// TODO: send ACK packet to the client
		//send ACK i+1
		ACK ack(i+1);
		ack.AckToChar(ack_buff);

		sendto(newsockfd, ack_buff, sizeof(ack_buff), 0, (struct sockaddr *)& client_addr, client_addr_len);
		i++;

	} while (recieved_packet_len < 516); // Have blocks left to be read from client (not end of transmission)

	fclose(pfile);

	printf("transmission finished\n");

	return 0;
}
