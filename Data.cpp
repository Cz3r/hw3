/*
 * Data.cpp
 *
 *  Created on: Jan 4, 2019
 *      Author: os
 */

#include <string.h>
#include "Data.h"

Data::Data():content(){}

bool Data::is_data_valid(char* data,int size)
{
	//check for valid length of data packet
	if(size<4 || size>516)
	{
		return false;
	}

	//check for valid opcode for data packet
	if(data[0]!=3 || data[1]!=0)
	{
		return false;
	}

	for(int i=0;i<2;i++)
	{
		this->opcode[i]=data[i];
		this->block_num[i]=data[i+2];
	}
	for(int i=4; i<size;i++)
	{
		this->content[i]=data[i];
	}

	return true;
}
Data::~Data() {
	// TODO Auto-generated destructor stub
}

