/*
 * WRQ.cpp
 *
 *  Created on: Jan 4, 2019
 *      Author: os
 */

#include <string.h>
#include <stdio.h>
#include "WRQ.h"

WRQ::WRQ() : file_name(), trans_mode() {
	opcode[0] = 2;
}

WRQ::~WRQ() {
}

bool WRQ::CheckValidReq(char* packet, int len)
{
	//printf("sizeof(WRQ) = %d\n", sizeof(WRQ));

	//check that packet has opcode bytes and the opcode is 2
	if (len > 2 and len > 4 and packet[0] == 2)
	{
		//packet is a write request
		char* new_file_name;
		new_file_name[0]='\0';
		char* new_trans_mode;
		new_trans_mode[0]='\0';
		char a = packet[2];
		int i = 3;

		//extract file_name from recieved string
		while (a != '\0')
		{
			strcat(new_file_name, &a);
			a = packet[i];
			i++;
		}
		strcat(new_file_name, "\0");

		strcpy(file_name, new_file_name);
		strcpy(trans_mode, packet+i);
		//check that trans_mode is octet
		if (strcmp(packet+i, "octet") != 0)
		{
			printf("ERROR wrq not octet type\n");
		}
		opcode[0] = 2;

		printf("file_name: %s\n", file_name);
		printf("trans_mode: %s\n", trans_mode);
		printf("opcode: %s\n", opcode);

		//if the packet is valid, this will have all the packet fields
		return true;
	}

	return false;
}
