/*
 * ACK.h
 *
 *  Created on: Jan 4, 2019
 *      Author: os
 */

#ifndef ACK_H_
#define ACK_H_

class ACK {
public:
	ACK(int block_num);
	virtual ~ACK();
	void AckToChar(char* dest);


	char opcode[2];
	char block_num[2];

} __attribute__((packed));

#endif /* ACK_H_ */
