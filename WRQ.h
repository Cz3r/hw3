/*
 * WRQ.h
 *
 *  Created on: Jan 4, 2019
 *      Author: os
 */

#ifndef WRQ_H_
#define WRQ_H_
#include <string.h>

using namespace std;

class WRQ {
public:
	WRQ();
	virtual ~WRQ();

	bool CheckValidReq(char* packet, int len);

	char opcode[2];
	char* file_name;
	char* trans_mode;


} __attribute__((packed));

#endif /* WRQ_H_ */
