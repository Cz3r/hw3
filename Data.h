/*
 * Data.h
 *
 *  Created on: Jan 4, 2019
 *      Author: os
 */

#ifndef DATA_H_
#define DATA_H_

class Data {
public:
	char opcode[2];
	char block_num[2];
	char* content;
	Data();
	bool is_data_valid(char*,int);
	~Data();
}__attribute__((packed));;

#endif /* DATA_H_ */
