/*
 * ACK.cpp
 *
 *  Created on: Jan 4, 2019
 *      Author: os
 */

#include <string.h>
#include "ACK.h"

ACK::ACK(int block) {
	opcode[0] = 4;
	opcode[1] = 0;
	block_num[0] = block and 0xF;
	block_num[1] = block and 0xF0;
}

ACK::~ACK() {
}

void ACK::AckToChar(char* dest)
{
	char ch[5];
	ch[0] = opcode[0];
	ch[1] = opcode[1];
	ch[2] = block_num[0];
	ch[3] = block_num[1];
	ch[4] = '\0';

	strcpy(dest, ch);
}

