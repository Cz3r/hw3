#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//#include "WRQ.h"
//#include "ACK.h"
//#include "Data.h"

#define _BSD_SOURCE

//ACK packet struct
typedef struct ACK{
	unsigned short opcode;
	unsigned short block_num;
}__attribute__((packed)) ACK;

// WRQ packet struct
typedef struct WRQ{
	unsigned short opcode;
	char* file_name;
	char* trans_mode;
}__attribute__((packed)) WRQ;

//Data packet struct
typedef struct Data{
	unsigned short opcode;
	unsigned short block_num;
	char* content;
}Data;


// function name: set_WRQ
// Description: set WRQ packet struct from buffer
// Parameters: pw - pointer to WRQ to be initialized , buff- pointer to the start of the recieved packe , size- buff size
// Returns: 0-success , -1-opcode does not match -2-size is to small (to avoid segmentaion fault)
int set_WRQ(WRQ* pw,char* buff,int size)
{
	if(size < 7)
	{
		return -2;
	}
	unsigned short* popcode=(unsigned short*)buff;
	pw->opcode = ntohs(*popcode);
	if(pw->opcode != 2)
	{
		return -1;
	}
	pw->file_name=buff+2;
	pw->trans_mode = buff+(strlen(pw->file_name)+3);
	return 0;
}

// function name: set_ACK
// Description: set ACK packet struct from buffer
// Parameters: pa - pointer to ACK to be initialized , bnum- block number
// Returns:NONE
void set_ACK(ACK* pa,int bnum)
{
	pa->block_num=htons(bnum);
	pa->opcode=htons(4);
}

// function name: set_Data
// Description: set data packet struct from buffer
// Parameters: pd - pointer to Data to be initialized , buff- pointer to the start of the recieved packet , size- buff size
// Returns: NONE
void set_Data(Data* pd,char* buff,int size)
{
	unsigned short* popcode=(unsigned short*)buff;
	unsigned short* bnum=(unsigned short*)(buff+2);
	pd->opcode=ntohs(*popcode);
	pd->block_num=ntohs(*bnum);
	pd->content = buff+4;

}

int main(int argc, char* argv[])
{

	const int WAIT_FOR_PACKET_TIMEOUT = 3;
	const int NUMBER_OF_FAILURES = 7;

	int error_count = 0;
	unsigned short port_number = 0;
	int sockfd;
	int recv_len;
	char read_buff[516];
	const size_t buf_len = sizeof(read_buff);
	char ack_buff[5] = {0};
	int timeoutExpiredCount = 0;
	unsigned int client_addr_len;
	struct sockaddr_in client_addr;
	struct sockaddr_in server_addr;
	FILE* file;
	int write_len;
	int i;
	int res;

	Data dt;
	WRQ req;
	ACK ack;

	if (argc != 2)
	{
		printf("TFTPEROOR: Invalid parameters\n");
		exit(1);
	}

	port_number = atoi(argv[1]);

	//configure socket
	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sockfd == -1)
	{
		printf("TTFTP_ERROR:socket()\n");
		perror(NULL);
		printf("RECVFAIL\n");
		exit(1);
	}
	memset(&server_addr, 0, sizeof(server_addr));

	//bind
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(port_number);

	int bind_return = bind(sockfd, (struct sockaddr *)& server_addr, sizeof(server_addr));

	if (bind_return < 0)//bind failed
	{
		printf("TTFTP_ERROR:bind()");
		perror(NULL);
		printf("RECVFAIL\n");
		exit(1);
	}
	client_addr_len=sizeof(client_addr);
	//wait for WRQ

	while(1){
		i=0;
		recv_len = recvfrom(sockfd, read_buff, buf_len, 0,(struct sockaddr *) &client_addr, &client_addr_len);
		if(recv_len <0)
		{
			close(sockfd);
			printf("TTFTP_ERROR:recvfrom()\n");
			perror(NULL);
			printf("RECVFAIL\n");
			exit(1);
		}
		//WRQ recieved
		res = set_WRQ(&req,read_buff,sizeof(read_buff));
		if(res == -1)//unmatched opcode
		{
			printf("FLOWERROR: WRQ packet should be with opcode 2 not %2d. packet is not WRQ\n",req.opcode);
			printf("RECVFAIL\n");
			exit(1);
		}
		else if(res == -2)//WRQ Size is invalid
		{
			close(sockfd);
			printf("FLOWERROR: size of WRQ packet is too small\n");
			printf("RECVFAIL\n");
			exit(1);

		}

		printf("IN:WRQ,%s,%s\n",req.file_name,req.trans_mode);

		//open file to write
		file = fopen(req.file_name, "w");
		if(file == NULL)
		{
			close(sockfd);
			printf("TFTPERROR:fopen()\n");
			perror(NULL);
			printf("RECVFAIL\n");
			exit(1);
		}


		//creating ACK package
		set_ACK(&ack,0);
		res = sendto(sockfd, &ack, sizeof(ack), 0, (struct sockaddr *)& client_addr, client_addr_len);
		if (res == -1)
		{
			close(sockfd);
			printf("TFTPERROR:sendto()\n");
			perror(NULL);
			printf("RECVFAIL\n");
			exit(1);
		}
		printf("OUT:ACK,%2d\n",i);
		//Conection was established and ack0 was send.




		fd_set rfds;
		struct timeval tv;
		do
			{

				do
				{
					do
					{
						recv_len=0;
						//checking for failure limitation


						// TODO: Wait WAIT_FOR_PACKET_TIMEOUT to see if something appears
						// for us at the socket (we are waiting for DATA)
						FD_ZERO(&rfds);
						FD_SET(sockfd, &rfds);
						tv.tv_sec = WAIT_FOR_PACKET_TIMEOUT;
						tv.tv_usec = 0;
						int select_return = select(sockfd+1, &rfds, NULL, NULL, &tv);

						if (select_return == -1)
						{
							close(sockfd);
							printf("TFTP_ERROR:select()\n");
							perror(NULL);
							printf("RECVFAIL\n");
							exit(1);
						}

						else if (select_return == 0) // TODO: Time out expired while waiting for data
						// to appear at the socket
						{

							timeoutExpiredCount++;
							if (timeoutExpiredCount>= NUMBER_OF_FAILURES)
							{
								// FATAL ERROR BAIL OUT
								printf("FLOWERROR:Failures exceeded the allowed number\n");
								printf("RECVFAIL\n");
								exit(1);
							}
							//send ACK i
							sendto(sockfd, &ack, sizeof(ack), 0, (struct sockaddr *)& client_addr, client_addr_len);
							printf("OUT:ACK,%2d\n",i);
						}

						//if there was something at the socket and
						// we are here not because of a timeout
						else
						{

							memset(&read_buff, 0, sizeof(read_buff));
							memset(&dt, 0, sizeof(dt));

							recv_len = recvfrom(sockfd, read_buff, sizeof(read_buff), 0, (struct sockaddr *)& client_addr, &client_addr_len);
							set_Data(&dt,read_buff,recv_len);

						}


					} while (recv_len <= 0); // TODO: Continue while some socket was ready
					// but recvfrom somehow failed to read the data

					if (dt.opcode != 3) // TODO: We got something else but DATA
					{
						// FATAL ERROR BAIL OUT
						close(sockfd);
						printf("FLOWERROR:packet opcode is %d insted of 3. packet is not data\n",dt.opcode);
						printf("RECVFAIL\n");
						exit(1);
					}

					if (dt.block_num != i+1) // The incoming block number is not what we have
					// expected, i.e. this is a DATA pkt but the block number
					// in DATA was wrong (not last ACK’s block number + 1)
					{
						// FATAL ERROR BAIL OUT
						close(sockfd);
						printf("FLOWERROR: expected block_num: %d but recived: %2d\n",i+1,dt.block_num);
						printf("RECVFAIL\n");
						exit(1);
					}
				} while (0);
				printf("IN:DATA,%2d,%d\n",dt.block_num,recv_len);
				timeoutExpiredCount = 0;
				write_len = fwrite(dt.content, recv_len-4 ,sizeof(char), file); // Data is perfectly fine so write next bulk of data
				if(write_len<=0)
				{
					close(sockfd);
					printf("TFTP_ERROR:fwrite()\n");
					perror(NULL);
					printf("RECVFAIL\n");
					exit(1);
				}
				printf("WRITING:%d\n",write_len);

				//	//creating ACK i+1
				set_ACK(&ack,i+1);
				res = sendto(sockfd, &ack, sizeof(ack_buff), 0, (struct sockaddr *)& client_addr, client_addr_len);
				if (res == -1)
				{
					close(sockfd);
					printf("TFTPERROR:sendto()\n");
					perror(NULL);
					printf("RECVFAIL");
				}
				printf("OUT:ACK,%2d\n",i+1);
				i++;

			} while (recv_len == 516); // Have blocks left to be read from client (not end of transmission)

			fclose(file);
			printf("RECVOK\n");
		}

}
